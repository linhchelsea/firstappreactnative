import React from 'react';
import { AppRegistry, FlatList, Text, View, ActivityIndicator, StyleSheet } from 'react-native';

export default class App extends React.Component {
  constructor (props) {
    super(props);
    this.state = { isLoading: true };
  }

  async componentDidMount () {
    const result = await fetch('https://facebook.github.io/react-native/movies.json');
    const resultJson = await result.json();
    this.setState({
      isLoading: false,
      dataSource: resultJson.movies,
    });
    console.log(resultJson);
    return resultJson;
  }

  render () {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator/>
        </View>
      );
    }
    return (
      <View style={ styles.containerFlat }>
        <FlatList
          data={ this.state.dataSource }
          renderItem={ ({ item }) => {
            return <Text>{ item.title} - { item.releaseYear }</Text>;
          }}
          keyExtractor={ ({ id }, index) => id }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   padding: 20,
  },
  containerFlat: {
   flex: 1,
   paddingTop: 20,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

AppRegistry.registerComponent('FirstApp', () => App);
