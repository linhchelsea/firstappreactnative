import React, { Component } from 'react';
import { TouchableHighlight, TouchableOpacity, Platform, TouchableNativeFeedback, TouchableWithoutFeedback,
  AppRegistry, StyleSheet, Text, Alert, Button, View } from 'react-native';

class Greeting extends Component {
  constructor (props) {
    super(props);
    this.state = { isShowingText: true };

    setInterval(() => {
      this.setState(previousState => {
        return {
          isShowingText: !previousState.isShowingText,
        };
      });
    }, 1000);
  }

  render () {
    let display = this.state.isShowingText ? this.props.text : ' ';
    return (
       <Text style={ this.props.style }>{ display }</Text>
    );
  }

}

export default class App extends React.Component {
  _onPressButton () {
    Alert.alert('You tapped the button');
  }

  _onLongPressButton () {
    Alert.alert('You long-pressed the button');
  }

  render() {
    return (
      <View style = { styles.container }>
        <TouchableHighlight onPress={ this._onPressButton } underlayColor='white'>
          <View style={ styles.button }>
            <Text style={ styles.buttonText }>TouchableHighlight</Text>
          </View>
        </TouchableHighlight>
        <TouchableOpacity onPress={ this._onPressButton }>
          <View style={ styles.button }>
            <Text style={ styles.buttonText }>TouchableOpacity</Text>
          </View>
        </TouchableOpacity>
        <TouchableNativeFeedback
          onPress={ this._onPressButton }
          background={ Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : '' }
        >
          <View style={ styles.button }>
            <Text style={ styles.buttonText }>TouchableNativeFeedback</Text>
          </View>
        </TouchableNativeFeedback>
        <TouchableWithoutFeedback
          onPress={ this._onPressButton }
          onLongPress={ this._onLongPressButton }
          underlayColor='#ffffff'
        >
          <View style={ styles.button }>
            <Text style={ styles.buttonText }>Touchable without feedback</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableHighlight
          onPress={ this._onPressButton }
          onLongPress={ this._onLongPressButton }
          underlayColor='#ffffff'
        >
          <View style={ styles.button }>
            <Text style={ styles.buttonText }>Touchable with long press</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    alignItems: 'center',
  },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#2196f3',
  },
  buttonText: {
    padding: 20,
    color: '#ffffff'
  },
  buttonContainer: {
    margin: 20,
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});

AppRegistry.registerComponent('FirstApp', () => App);
